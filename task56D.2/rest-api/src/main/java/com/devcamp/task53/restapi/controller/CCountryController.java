package com.devcamp.task53.restapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task53.restapi.model.CCountry;
import com.devcamp.task53.restapi.model.CRegion;
import com.devcamp.task53.restapi.service.CCountryService;
import com.fasterxml.jackson.core.sym.Name;

@CrossOrigin(value = "*", maxAge = 1)
@RequestMapping("/")
@RestController
public class CCountryController {
    @Autowired
    static CCountryService countries;

    @GetMapping("/countries")
    public List<CCountry> getCountryList() throws Exception {
        List<CCountry> allCountries = CCountryService.getCountryList();

        return allCountries;
    }

    @GetMapping("/countries/{countrycode}")
    public Map<String, Object> getRegionOfCOuntry(@PathParam(value = "countrycode") int countrycode)
            throws Exception {
        Map<String, Object> returnObj = new HashMap<String, Object>();
        List<CRegion> regionList = null;
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < CCountryService.getCountryList().size()) {
            if (CCountryService.getCountryList().get(i).getCoutryCode() == countrycode) {
                regionList = CCountryService.getCountryList().get(i).getRegion();
                isFound = true;
                returnObj.put("regions", regionList);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("regions", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;

    }

    @PostMapping("/countries/addnew")
    public CCountry createNewCountry(@RequestBody CCountry newCountry) {
        System.out.println(CCountryService.getCountryList().add(newCountry));
        CCountryService.getCountryList().add(newCountry);
        return newCountry;
    }
}
