package com.devcamp.task53.restapi.service;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.task53.restapi.model.CRegion;

public class CRegionService {

    private static List<CRegion> regionVietNam = new ArrayList<CRegion>();
    private static List<CRegion> regionUS = new ArrayList<CRegion>();
    private static List<CRegion> regionRussia = new ArrayList<CRegion>();

    static {
        CRegion dongnai = new CRegion("Đồng Nai", 60);
        CRegion tpHCM = new CRegion("Sài Gòn", 51);
        CRegion hanoi = new CRegion("Hà Nội", 29);
        CRegion texas = new CRegion("Texas", 29);
        CRegion florida = new CRegion("Florida", 29);
        CRegion newyork = new CRegion("New York", 29);
        CRegion moscow = new CRegion("Moscow", 29);
        CRegion kaluga = new CRegion("Kaluga", 29);
        CRegion saintpeter = new CRegion("Saint Peterburg", 29);

        regionVietNam.add(dongnai);
        regionVietNam.add(tpHCM);
        regionVietNam.add(hanoi);
        regionUS.add(texas);
        regionUS.add(florida);
        regionUS.add(newyork);
        regionRussia.add(moscow);
        regionRussia.add(kaluga);
        regionRussia.add(saintpeter);

    }

    public static List<CRegion> getRegionVietNam() {
        return regionVietNam;
    }

    public static List<CRegion> getRegionUS() {
        return regionUS;
    }

    public static List<CRegion> getRegionRussia() {
        return regionRussia;
    }

    public static void setRegionVietNam(List<CRegion> regionVietNam) {
        CRegionService.regionVietNam = regionVietNam;
    }

    public static void setRegionUS(List<CRegion> regionUS) {
        CRegionService.regionUS = regionUS;
    }

    public static void setRegionRussia(List<CRegion> regionRussia) {
        CRegionService.regionRussia = regionRussia;
    }

}
