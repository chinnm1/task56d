package com.devcamp.task53.restapi.model;

import java.util.List;

public class CCountry {
    private String countryName;
    private int coutryCode;
    private List<CRegion> region;

    public CCountry(String countryName, int coutryCode, List<CRegion> region) {
        this.countryName = countryName;
        this.coutryCode = coutryCode;
        this.region = region;
    }

    public CCountry() {
    }

    public String getCountryName() {
        return countryName;
    }

    public int getCoutryCode() {
        return coutryCode;
    }

    public List<CRegion> getRegion() {
        return region;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setCoutryCode(int coutryCode) {
        this.coutryCode = coutryCode;
    }

    public void setRegion(List<CRegion> region) {
        this.region = region;
    }

}
