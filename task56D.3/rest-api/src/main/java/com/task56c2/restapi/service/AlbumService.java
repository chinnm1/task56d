package com.task56c2.restapi.service;

import java.util.ArrayList;
import java.util.List;

import com.task56c2.restapi.model.Album;

public class AlbumService {
    private static List<Album> listalbum1 = new ArrayList<Album>();
    private static List<Album> listalbum2 = new ArrayList<Album>();
    private static List<Album> listalbum3 = new ArrayList<Album>();
    static {
        ArrayList<String> listSong1 = new ArrayList<>();
        listSong1.add("listSong1-bai1");
        listSong1.add("listSong1-bai2");
        listSong1.add("listSong1-bai3");
        ArrayList<String> listSong2 = new ArrayList<>();
        listSong2.add("listSong2-bai1");
        listSong2.add("listSong2-bai2");
        listSong2.add("listSong2-bai3");
        ArrayList<String> listSong3 = new ArrayList<>();
        listSong3.add("listSong3-bai1");
        listSong3.add("listSong3-bai2");
        listSong3.add("listSong3-bai3");

        Album album1 = new Album(001, "album1", listSong1);
        Album album2 = new Album(002, "album2", listSong2);
        Album album3 = new Album(003, "album3", listSong3);

        listalbum1.add(album1);
        listalbum2.add(album2);
        listalbum3.add(album3);

    }

    public static List<Album> getListalbum1() {
        return listalbum1;
    }

    public static List<Album> getListalbum2() {
        return listalbum2;
    }

    public static List<Album> getListalbum3() {
        return listalbum3;
    }

    public static void setListalbum1(List<Album> listalbum1) {
        AlbumService.listalbum1 = listalbum1;
    }

    public static void setListalbum2(List<Album> listalbum2) {
        AlbumService.listalbum2 = listalbum2;
    }

    public static void setListalbum3(List<Album> listalbum3) {
        AlbumService.listalbum3 = listalbum3;
    }
}
