package com.task56c2.restapi.service;

import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.synth.Region;

import org.springframework.beans.factory.annotation.Autowired;

import com.task56c2.restapi.model.Album;
import com.task56c2.restapi.model.Artist;

public class ArtistService {

    private static List<Artist> artistsList = new ArrayList<Artist>();
    @Autowired
    static AlbumService album;

    public ArtistService() {
    }

    static {
        Artist artist1 = new Artist(01, "artist1", null);
        Artist artist2 = new Artist(02, "artist2", null);
        Artist artist3 = new Artist(03, "artist3", null);
        artistsList.add(artist1);
        artistsList.add(artist2);
        artistsList.add(artist3);
        for (int i = 0; i < artistsList.size(); i++) {
            ArrayList<Album> asdas = new ArrayList<>();
            if (artistsList.get(i).getName() == "artist1") {
                artistsList.get(i).setAlbums(AlbumService.getListalbum1());
            } else if (artistsList.get(i).getName() == "artist2") {
                artistsList.get(i).setAlbums(AlbumService.getListalbum2());
            } else if (artistsList.get(i).getName() == "artist3") {
                artistsList.get(i).setAlbums(AlbumService.getListalbum3());
            }
        }
    }

    public static List<Artist> getArtistsList() {
        return artistsList;
    }

    public static void setArtistsList(List<Artist> artistsList) {
        ArtistService.artistsList = artistsList;
    }

}
