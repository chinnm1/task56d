package com.task56c2.restapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56c2.restapi.model.Album;
import com.task56c2.restapi.service.ArtistService;

@RestController
public class AlbumController {
    @CrossOrigin
    @GetMapping("album-info")
    public Map<String, Object> getRegionOfCOuntry(@RequestParam(required = true, name = "albumId") int id)
            throws Exception {
        Map<String, Object> returnObj = new HashMap<String, Object>();
        List<Album> listAlbum = null;
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < ArtistService.getArtistsList().size()) {
            if (ArtistService.getArtistsList().get(i).getId() == id) {
                listAlbum = ArtistService.getArtistsList().get(i).getAlbums();
                isFound = true;
                returnObj.put("list album", listAlbum);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("list album", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;

    }

}
