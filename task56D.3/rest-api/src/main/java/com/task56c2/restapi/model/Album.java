package com.task56c2.restapi.model;

import java.util.ArrayList;

public class Album {

    public Album(int id, String name, ArrayList<String> songs) {
        this.id = id;
        this.name = name;
        this.songs = songs;
    }

    public Album(int id, String name) {
        this.id = id;
        this.name = name;
    }

    private int id;
    private String name;
    private ArrayList<String> songs;

    public Album() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getSongs() {
        return songs;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSongs(ArrayList<String> songs) {
        this.songs = songs;
    }

}
