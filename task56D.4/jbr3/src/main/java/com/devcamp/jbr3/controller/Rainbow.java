package com.devcamp.jbr3.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Rainbow {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowArr(@RequestParam(value = "username", defaultValue = "Pizza Lover") String name,
            @RequestParam(value = "username1", defaultValue = "Pizza Lover") String name1) {
        ArrayList<String> arrList = new ArrayList();
        arrList.add("red");
        arrList.add("orange");
        arrList.add("yellow");
        arrList.add("green");
        arrList.add("blue");
        arrList.add("indigo");
        arrList.add("violet");
        arrList.add(name);
        arrList.add(name1);
        return arrList;
    }

    @CrossOrigin
    @GetMapping("/rainbow-request-query")
    public Map<String, Object> getRainbow(@RequestParam(value = "pos") int pos) {
        ArrayList<Integer> arrList = new ArrayList<>();
        arrList.add(1);
        arrList.add(23);
        arrList.add(32);
        arrList.add(43);
        arrList.add(54);
        arrList.add(65);
        arrList.add(86);
        arrList.add(10);
        arrList.add(15);
        arrList.add(16);
        arrList.add(16);
        arrList.add(18);
        Map<String, Object> returnObj = new HashMap<String, Object>();
        ArrayList<Integer> filterList = new ArrayList<>();

        for (int i = 0; i < arrList.size(); i++) {
            if (arrList.get(i) > pos) {
                filterList.add(arrList.get(i));

                returnObj.put("number", filterList);
                returnObj.put("status", new String("ok"));

            } else {

                returnObj.put("number", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;
    }

    @GetMapping("/rainbow-request-param/{index}")
    public Map<String, Object> getRainbowByIndex(@PathVariable(value = "index") int index) {
        ArrayList<Integer> arrList = new ArrayList<>();
        arrList.add(1);
        arrList.add(23);
        arrList.add(32);
        arrList.add(43);
        arrList.add(54);
        arrList.add(65);
        arrList.add(86);
        arrList.add(10);
        arrList.add(15);
        arrList.add(16);
        arrList.add(16);
        arrList.add(18);
        Map<String, Object> returnObj = new HashMap<String, Object>();
        ArrayList<Integer> filterList = new ArrayList<>();
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < arrList.size()) {
            if (index >= 0 && index <= 10 && i == index) {
                filterList.add(arrList.get(index));
                isFound = true;
                returnObj.put("rainbows", filterList);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("rainbow", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;
    }

    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> getSplit(
            @RequestParam(value = "username", defaultValue = "Using Arrays.asList() method - Pass the required array to this method and get a List object and pass it as a parameter to the constructor of the ArrayList class.") String name) {
        ArrayList<String> arrList = new ArrayList();
        Collections.addAll(arrList, name.split(" ", 8));

        return arrList;
    }
}
