package com.task56b4.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.task56b4.restapi.model.Customer;
import com.task56b4.restapi.model.Invoice;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<Invoice> getListInvoice() {
        Customer customer1 = new Customer(1, "Lan", 20);
        Customer customer2 = new Customer(2, "Hoang", 10);
        Customer customer3 = new Customer(3, "Thuy", 15);

        Invoice invoice1 = new Invoice(01, customer1, 200000);
        Invoice invoice2 = new Invoice(02, customer1, 300000);
        Invoice invoice3 = new Invoice(03, customer1, 400000);
        ArrayList<Invoice> arrayList = new ArrayList<>();
        arrayList.add(invoice1);
        arrayList.add(invoice2);
        arrayList.add(invoice3);
        return arrayList;

    }

    @GetMapping("/invoices/{invoiceId}")
    public Map<String, Object> getRegionOfCOuntry(@PathVariable(required = true, name = "invoiceId") int invoiceId)
            throws Exception {
        Customer customer1 = new Customer(1, "Lan", 20);
        Customer customer2 = new Customer(2, "Hoang", 10);
        Customer customer3 = new Customer(3, "Thuy", 15);

        Invoice invoice1 = new Invoice(01, customer1, 200000);
        Invoice invoice2 = new Invoice(02, customer1, 300000);
        Invoice invoice3 = new Invoice(03, customer1, 400000);
        ArrayList<Invoice> arrayList = new ArrayList<>();
        arrayList.add(invoice1);
        arrayList.add(invoice2);
        arrayList.add(invoice3);
        Map<String, Object> returnObj = new HashMap<String, Object>();
        Invoice invoice = null;
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < arrayList.size()) {
            if (arrayList.get(i).getId() == invoiceId) {
                invoice = arrayList.get(i);
                isFound = true;
                returnObj.put("invoice", invoice);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("invoice", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;

    }

}
