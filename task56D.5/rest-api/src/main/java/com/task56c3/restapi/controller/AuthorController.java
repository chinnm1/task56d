package com.task56c3.restapi.controller;

import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56c3.restapi.model.Author;
import com.task56c3.restapi.service.AuthorService;
import com.task56c3.restapi.service.BookService;

@RestController
public class AuthorController {
    @CrossOrigin
    @GetMapping("/author-info")
    public Map<String, Object> getRegionOfCOuntry(@RequestParam String email)
            throws Exception {

        Map<String, Object> returnObj = new HashMap<String, Object>();
        Author author = null;
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < AuthorService.getAllListAuthor().size()) {

            System.out.println(AuthorService.getAllListAuthor().get(i).getEmail());
            if (AuthorService.getAllListAuthor().get(i).getEmail().equals(email)) {

                author = AuthorService.getAllListAuthor().get(i);
                isFound = true;
                returnObj.put("author", author);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("author", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;

    }

    @GetMapping("/author-gender")
    public Map<String, Object> getRegionOfCOuntry(@RequestParam(required = true, name = "authorGender") char gender)
            throws Exception {
        Map<String, Object> returnObj = new HashMap<String, Object>();
        List<Author> author = new ArrayList<Author>();

        for (int i = 0; i < AuthorService.getAllListAuthor().size(); i++) {
            if (AuthorService.getAllListAuthor().get(i).getGender() == gender) {
                // author.add(AuthorService.getAllListAuthor().get(i));
                author.add(AuthorService.getAllListAuthor().get(i));
                returnObj.put("author", author);
                returnObj.put("status", new String("ok"));

            } else {

                returnObj.put("author", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;

    }

}
