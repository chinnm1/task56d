package com.task56c3.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56c3.restapi.model.Book;
import com.task56c3.restapi.service.BookService;

@RestController
public class BookController {
    @CrossOrigin
    @GetMapping("/books")
    public List<Book> getbook() throws Exception {
        List<Book> allBook = BookService.getBookList();
        return allBook;
    }

    @GetMapping("/books/{bookId}")
    public Map<String, Object> getRegionOfCOuntry(@PathVariable(required = true, name = "bookId") int bookId)
            throws Exception {
        Map<String, Object> returnObj = new HashMap<String, Object>();
        Book book = null;
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < BookService.getBookList().size()) {
            if (i == bookId) {
                book = BookService.getBookList().get(i);
                isFound = true;
                returnObj.put("book", book);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("book", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;

    }

}
