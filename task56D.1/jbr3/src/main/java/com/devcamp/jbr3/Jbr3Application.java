package com.devcamp.jbr3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr3Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr3Application.class, args);
	}

}
