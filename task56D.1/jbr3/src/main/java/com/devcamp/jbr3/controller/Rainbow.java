package com.devcamp.jbr3.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Rainbow {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowArr(@RequestParam(value = "username", defaultValue = "Pizza Lover") String name,
            @RequestParam(value = "username1", defaultValue = "Pizza Lover") String name1) {
        ArrayList<String> arrList = new ArrayList();
        arrList.add("red");
        arrList.add("orange");
        arrList.add("yellow");
        arrList.add("green");
        arrList.add("blue");
        arrList.add("indigo");
        arrList.add("violet");
        arrList.add(name);
        arrList.add(name1);
        return arrList;
    }

    @CrossOrigin
    @GetMapping("/rainbow-request-query")
    public Map<String, Object> getRainbow(@RequestParam(value = "filter") String filterString) {
        ArrayList<String> arrList = new ArrayList();
        arrList.add("red");
        arrList.add("orange");
        arrList.add("yellow");
        arrList.add("green");
        arrList.add("blue");
        arrList.add("indigo");
        arrList.add("violet");
        Map<String, Object> returnObj = new HashMap<String, Object>();
        ArrayList<String> filterList = new ArrayList<>();
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < arrList.size()) {
            if (arrList.get(i).equals(filterString)) {
                filterList.add(filterString);
                isFound = true;
                returnObj.put("rainbows", filterList);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("rainbow", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;
    }

    @GetMapping("/rainbow-request-param/{index}")
    public Map<String, Object> getRainbowByIndex(@PathParam(value = "index") int index) {
        ArrayList<String> arrList = new ArrayList<>();
        arrList.add("red");
        arrList.add("orange");
        arrList.add("yellow");
        arrList.add("green");
        arrList.add("blue");
        arrList.add("indigo");
        arrList.add("violet");
        Map<String, Object> returnObj = new HashMap<String, Object>();
        ArrayList<String> filterList = new ArrayList<>();
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < arrList.size()) {
            if (index >= 0 && index <= 6) {
                filterList.add(arrList.get(index));
                isFound = true;
                returnObj.put("rainbows", filterList);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("rainbow", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;
    }

    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> getSplit(
            @RequestParam(value = "username", defaultValue = "Using Arrays.asList() method - Pass the required array to this method and get a List object and pass it as a parameter to the constructor of the ArrayList class.") String name) {
        ArrayList<String> arrList = new ArrayList();
        Collections.addAll(arrList, name.split(" ", 8));

        return arrList;
    }
}
